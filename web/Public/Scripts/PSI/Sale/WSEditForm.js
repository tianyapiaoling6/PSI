/**
 * 销售出库 - 新建或编辑界面
 * 
 * @author 艾格林门信息服务（大连）有限公司
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Sale.WSEditForm", {
  extend: "PSI.AFX.BaseDialogForm",
  config: {
    genBill: false,
    sobillRef: null
  },

  mixins: ["PSI.Mix.GoodsPrice"],

  /**
   * @override
   */
  initComponent() {
    var me = this;
    me.__readonly = false;
    var entity = me.getEntity();
    this.adding = entity == null;

    var title = entity == null ? "新建销售出库单" : "编辑销售出库单";
    title = me.formatTitle(title);

    PCL.apply(me, {
      header: {
        title: title,
        height: 40
      },
      maximized: true,
      width: 1000,
      height: 600,
      layout: "border",
      tbar: [{
        id: "displayFieldBarcode",
        value: "条码录入",
        xtype: "displayfield"
      }, {
        xtype: "textfield",
        cls: "PSI-toolbox",
        id: "editBarcode",
        listeners: {
          specialkey: {
            fn: me.onEditBarcodeKeydown,
            scope: me
          }
        }
      }, " ", {
        text: "保存",
        iconCls: "PSI-button-ok",
        handler: me.onOK,
        scope: me,
        id: "buttonSave"
      }, "-", {
        text: "取消",
        handler() {
          if (me.__readonly) {
            me.close();
            return;
          }

          me.confirm("请确认是否取消当前操作？", () => {
            me.close();
          });
        },
        scope: me,
        id: "buttonCancel"
      }, "->", {
        text: "表单通用操作指南",
        iconCls: "PSI-help",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=commBill"));
        }
      }, "-", {
        fieldLabel: "快捷访问",
        labelSeparator: "",
        margin: "5 5 5 0",
        cls: "PSI-toolbox",
        labelAlign: "right",
        labelWidth: 50,
        emptyText: "双击此处弹出选择框",
        xtype: "psi_mainmenushortcutfield"
      }],
      items: [{
        region: "center",
        border: 0,
        bodyPadding: 10,
        layout: "fit",
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        border: 0,
        layout: {
          type: "table",
          columns: 4
        },
        height: 90,
        bodyPadding: 10,
        items: [{
          xtype: "hidden",
          id: "hiddenId",
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: "editRef",
          fieldLabel: "单号",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          width: 175,
          xtype: "displayfield",
          value: me.toFieldNoteText("保存后自动生成")
        }, {
          id: "editBizDT",
          fieldLabel: "业务日期",
          allowBlank: false,
          blankText: "没有输入业务日期",
          labelAlign: "right",
          labelSeparator: "",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.onEditBizDTSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editBizUser",
          fieldLabel: "业务员",
          xtype: "psi_userfield",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          allowBlank: false,
          blankText: "没有输入业务员",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.onEditBizUserSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editWarehouse",
          fieldLabel: "出库仓库",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "psi_warehousefield",
          fid: "2002",
          allowBlank: false,
          blankText: "没有输入出库仓库",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          width: 430,
          listeners: {
            specialkey: {
              fn: me.onEditWarehouseSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editCustomer",
          xtype: "psi_customerfield",
          fieldLabel: "客户",
          showAddButton: true,
          allowBlank: false,
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          colspan: 2,
          width: 430,
          blankText: "没有输入客户",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.onEditCustomerSpecialKey,
              scope: me
            }
          },
          callbackFunc: me.__setCustomerExtData
        }, {
          id: "editReceivingType",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "收款方式",
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [["0", "记应收账款"],
            ["1", "现金收款"],
            ["2", "用预收款支付"]]
          }),
          value: "0",
          listeners: {
            specialkey: {
              fn: me.onEditReceivingTypeSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editDealAddress",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "送货地址",
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me.onEditDealAddressSpecialKey,
              scope: me
            }
          },
          width: 430
        }, {
          id: "editBillMemo",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "备注",
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me.onEditBillMemoSpecialKey,
              scope: me
            }
          },
          colspan: 3,
          width: 645
        }]
      }],
      listeners: {
        show: {
          fn: me.onWndShow,
          scope: me
        },
        close: {
          fn: me.onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editRef = PCL.getCmp("editRef");
    me.editBizDT = PCL.getCmp("editBizDT");
    me.editBizUser = PCL.getCmp("editBizUser");
    me.editWarehouse = PCL.getCmp("editWarehouse");
    me.editCustomer = PCL.getCmp("editCustomer");
    me.editReceivingType = PCL.getCmp("editReceivingType");
    me.editDealAddress = PCL.getCmp("editDealAddress");
    me.editBillMemo = PCL.getCmp("editBillMemo");
  },

  onWindowBeforeUnload(e) {
    return (window.event.returnValue = e.returnValue = '确认离开当前页面？');
  },

  onWndClose() {
    PCL.WindowManager.hideAll();

    PCL.get(window).un('beforeunload', this.onWindowBeforeUnload);
  },

  onWndShow() {
    PCL.get(window).on('beforeunload', this.onWindowBeforeUnload);

    var me = this;

    me.editWarehouse.focus();

    me.__canEditGoodsPrice = false;
    var el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    PCL.Ajax.request({
      url: PSI.Const.BASE_URL + "Home/Sale/wsBillInfo",
      params: {
        id: PCL.getCmp("hiddenId").getValue(),
        sobillRef: me.getSobillRef()
      },
      method: "POST",
      callback(options, success, response) {
        el.unmask();

        if (success) {
          var data = PCL.JSON.decode(response.responseText);

          if (data.canEditGoodsPrice) {
            me.__canEditGoodsPrice = true;
            PCL.getCmp("columnGoodsPrice").setEditor({
              xtype: "numberfield",
              allowDecimals: true,
              hideTrigger: true
            });
            PCL.getCmp("columnGoodsMoney").setEditor({
              xtype: "numberfield",
              allowDecimals: true,
              hideTrigger: true
            });
          }

          if (me.getGenBill()) {
            // 从销售订单生成销售出库单
            PCL.getCmp("editCustomer").setIdValue(data.customerId);
            PCL.getCmp("editCustomer").setValue(data.customerName);
            PCL.getCmp("editBizUser").setIdValue(data.bizUserId);
            PCL.getCmp("editBizUser").setValue(data.bizUserName);
            PCL.getCmp("editBizDT").setValue(data.dealDate);
            PCL.getCmp("editReceivingType").setValue(data.receivingType);
            PCL.getCmp("editBillMemo").setValue(data.memo);

            me.editDealAddress.setValue(data.dealAddress);

            var store = me.getGoodsGrid().getStore();
            store.removeAll();
            store.add(data.items);

            PCL.getCmp("editCustomer").setReadOnly(true);
            PCL.getCmp("columnActionDelete").hide();
            PCL.getCmp("columnActionAdd").hide();
            PCL.getCmp("columnActionAppend").hide();

            PCL.getCmp("editBarcode").setDisabled(true);

            if (data.warehouseId) {
              var editWarehouse = PCL.getCmp("editWarehouse");
              editWarehouse.setIdValue(data.warehouseId);
              editWarehouse.setValue(data.warehouseName);
            }
          } else {

            if (data.ref) {
              PCL.getCmp("editRef").setValue(me.toFieldNoteText(data.ref));
            }

            PCL.getCmp("editCustomer").setIdValue(data.customerId);
            PCL.getCmp("editCustomer").setValue(data.customerName);
            PCL.getCmp("editCustomer").setShowAddButton(data.showAddCustomerButton);

            PCL.getCmp("editWarehouse").setIdValue(data.warehouseId);
            PCL.getCmp("editWarehouse").setValue(data.warehouseName);

            PCL.getCmp("editBizUser").setIdValue(data.bizUserId);
            PCL.getCmp("editBizUser").setValue(data.bizUserName);
            if (data.bizDT) {
              PCL.getCmp("editBizDT").setValue(data.bizDT);
            }
            if (data.receivingType) {
              PCL.getCmp("editReceivingType").setValue(data.receivingType);
            }
            if (data.memo) {
              PCL.getCmp("editBillMemo").setValue(data.memo);
            }
            me.editDealAddress.setValue(data.dealAddress);

            var store = me.getGoodsGrid().getStore();
            store.removeAll();
            if (data.items) {
              store.add(data.items);
            }
            if (store.getCount() == 0) {
              store.add({});
            }

            if (data.billStatus && data.billStatus != 0) {
              me.setBillReadonly();
            }
          }
        } else {
          me.showInfo("网络错误")
        }
      }
    });
  },

  onOK() {
    var me = this;
    PCL.getBody().mask("正在保存中...");
    PCL.Ajax.request({
      url: PSI.Const.BASE_URL + "Home/Sale/editWSBill",
      method: "POST",
      params: {
        adding: me.adding ? "1" : "0",
        jsonStr: me.getSaveData(),
        checkInv: 1
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          var data = PCL.JSON.decode(response.responseText);
          if (data.success) {
            me.close();
            var pf = me.getParentForm();
            if (pf) {
              pf.refreshMainGrid(data.id);
            }
            me.tip("成功保存数据");
          } else {
            if (data.checkInv == "1") {
              // 检查到库存不足，提醒用户
              me.confirm(data.msg, () => {
                PCL.Ajax.request({
                  url: PSI.Const.BASE_URL + "Home/Sale/editWSBill",
                  method: "POST",
                  params: {
                    jsonStr: me.getSaveData(),
                    checkInv: 0
                  },
                  callback(options, success, response) {
                    PCL.getBody().unmask();

                    if (success) {
                      me.close();
                      var pf = me.getParentForm();
                      if (pf) {
                        pf.refreshMainGrid(data.id);
                      }
                      me.tip("成功保存数据");
                    } else {
                      me.showInfo(data.msg);
                    }
                  }
                });
              })
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      }
    });
  },

  onEditBizDTSpecialKey(field, e) {
    var me = this;

    if (e.getKey() == e.ENTER) {
      me.editBizUser.focus();
    }
  },

  onEditCustomerSpecialKey(field, e) {
    var me = this;

    if (e.getKey() == e.ENTER) {
      me.editReceivingType.focus();
    }
  },

  onEditWarehouseSpecialKey(field, e) {
    var me = this;

    if (e.getKey() == e.ENTER) {
      me.editCustomer.focus();
    }
  },

  onEditBizUserSpecialKey(field, e) {
    var me = this;

    if (me.__readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      me.editWarehouse.focus();
    }
  },

  onEditReceivingTypeSpecialKey(field, e) {
    var me = this;

    if (me.__readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      me.editDealAddress.focus();
    }
  },

  onEditDealAddressSpecialKey(field, e) {
    var me = this;

    if (me.__readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      me.editBillMemo.focus();
    }
  },

  onEditBillMemoSpecialKey(field, e) {
    if (this.__readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      var me = this;
      var store = me.getGoodsGrid().getStore();
      if (store.getCount() == 0) {
        store.add({});
      }
      me.getGoodsGrid().focus();
      me.__cellEditing.startEdit(0, 1);
    }
  },

  getGoodsGrid() {
    var me = this;
    if (me.__goodsGrid) {
      return me.__goodsGrid;
    }
    PCL.define("PSIWSBillDetail_EditForm", {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode", "goodsName",
        "goodsSpec", "unitName", "goodsCount", {
          name: "goodsMoney",
          type: "float"
        }, "goodsPrice", "sn", "memo", "soBillDetailId", {
          name: "taxRate",
          type: "int"
        }, {
          name: "tax",
          type: "float"
        }, {
          name: "moneyWithTax",
          type: "float"
        }, "goodsPriceWithTax"]
    });
    var store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: "PSIWSBillDetail_EditForm",
      data: []
    });

    me.__cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me.cellEditingAfterEdit,
          scope: me
        }
      }
    });

    me.__goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true,
        markDirty: !me.adding
      },
      features: [{
        ftype: "summary"
      }],
      plugins: [me.__cellEditing],
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 30
      }), {
        header: "商品编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "psi_goods_with_saleprice_field",
          parentCmp: me,
          editCustomerName: "editCustomer",
          editWarehouseName: "editWarehouse"
        }
      }, {
        menuDisabled: true,
        draggable: false,
        sortable: false,
        header: "品名/规格型号",
        dataIndex: "goodsName",
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "销售数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        draggable: false,
        sortable: false,
        align: "right",
        width: 90,
        editor: {
          xtype: "numberfield",
          allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
          decimalPrecision: PSI.Const.GC_DEC_NUMBER,
          minValue: 0,
          hideTrigger: true
        }
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 60,
        align: "center"
      }, {
        header: "销售单价",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        draggable: false,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        id: "columnGoodsPrice",
        summaryRenderer() {
          return "金额合计";
        }
      }, {
        header: "销售金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        draggable: false,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        id: "columnGoodsMoney",
        summaryType: "sum"
      }, {
        header: "含税价",
        dataIndex: "goodsPriceWithTax",
        menuDisabled: true,
        draggable: false,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        }
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        align: "right",
        format: "0",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 60
      }, {
        header: "税金",
        dataIndex: "tax",
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "序列号",
        dataIndex: "sn",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "textfield"
        }
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "textfield"
        }
      }, {
        header: "",
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 50,
        xtype: "actioncolumn",
        id: "columnActionDelete",
        items: [{
          icon: PSI.Const.BASE_URL
            + "Public/Images/icons/delete.png",
          tooltip: "删除当前记录",
          handler(grid, row) {
            var store = grid.getStore();
            store.remove(store.getAt(row));
            if (store.getCount() == 0) {
              store.add({});
            }
          },
          scope: me
        }]
      }, {
        header: "",
        id: "columnActionAdd",
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 50,
        xtype: "actioncolumn",
        items: [{
          icon: PSI.Const.BASE_URL
            + "Public/Images/icons/insert.png",
          tooltip: "在当前记录之前插入新记录",
          handler(grid, row) {
            var store = grid.getStore();
            store.insert(row, [{}]);
          },
          scope: me
        }]
      }, {
        header: "",
        id: "columnActionAppend",
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 50,
        xtype: "actioncolumn",
        items: [{
          icon: PSI.Const.BASE_URL
            + "Public/Images/icons/add.png",
          tooltip: "在当前记录之后新增记录",
          handler(grid, row) {
            var store = grid.getStore();
            store.insert(row + 1, [{}]);
          },
          scope: me
        }]
      }],
      store: store,
      listeners: {
        cellclick() {
          return !me.__readonly;
        }
      }
    });

    return me.__goodsGrid;
  },

  cellEditingAfterEdit(editor, e) {
    var me = this;

    var fieldName = e.field;
    var goods = e.record;
    var oldValue = e.originalValue;
    if (fieldName == "goodsCount") {
      if (goods.get(fieldName) != oldValue) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsPrice") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsMoney") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcPrice(goods);
      }
    } else if (fieldName == "memo") {
      if (me.getGenBill()) {
        // 从销售订单生成入库单的时候不能新增明细记录
        return;
      }

      var store = me.getGoodsGrid().getStore();
      if (e.rowIdx == store.getCount() - 1) {
        store.add({});
        me.getGoodsGrid().getSelectionModel().select(e.rowIdx + 1);
        me.__cellEditing.startEdit(e.rowIdx + 1, 1);
      }
    } else if (fieldName == "moneyWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcTax(goods);
      }
    } else if (fieldName == "tax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoneyWithTax(goods);
      }
    } else if (fieldName == "goodsPriceWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney2(goods);
      }
    }
  },

  // xtype:psi_goods_with_saleprice_field回调本方法
  // 参见PSI.Goods.GoodsWithSalePriceField的onOK方法
  __setGoodsInfo(data) {
    var me = this;
    var item = me.getGoodsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    var goods = item[0];

    goods.set("goodsId", data.id);
    goods.set("goodsCode", data.code);
    goods.set("goodsName", data.name);
    goods.set("unitName", data.unitName);
    goods.set("goodsSpec", data.spec);
    goods.set("goodsPrice", data.salePrice);

    goods.set("taxRate", data.taxRate);

    me.calcMoney(goods);
  },

  getSaveData() {
    var me = this;

    var result = {
      id: PCL.getCmp("hiddenId").getValue(),
      bizDT: PCL.Date.format(PCL.getCmp("editBizDT").getValue(), "Y-m-d"),
      customerId: PCL.getCmp("editCustomer").getIdValue(),
      warehouseId: PCL.getCmp("editWarehouse").getIdValue(),
      bizUserId: PCL.getCmp("editBizUser").getIdValue(),
      receivingType: PCL.getCmp("editReceivingType").getValue(),
      billMemo: PCL.getCmp("editBillMemo").getValue(),
      sobillRef: me.getSobillRef(),
      dealAddress: me.editDealAddress.getValue(),
      items: []
    };

    var store = this.getGoodsGrid().getStore();
    for (var i = 0; i < store.getCount(); i++) {
      var item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        goodsId: item.get("goodsId"),
        goodsCount: item.get("goodsCount"),
        goodsPrice: item.get("goodsPrice"),
        goodsMoney: item.get("goodsMoney"),
        sn: item.get("sn"),
        memo: item.get("memo"),
        soBillDetailId: item.get("soBillDetailId"),
        taxRate: item.get("taxRate"),
        tax: item.get("tax"),
        moneyWithTax: item.get("moneyWithTax"),
        goodsPriceWithTax: item.get("goodsPriceWithTax")
      });
    }

    return PCL.JSON.encode(result);
  },

  setBillReadonly() {
    var me = this;
    me.__readonly = true;
    me.setTitle("<span style='font-size:160%'>查看销售出库单</span>");
    PCL.getCmp("displayFieldBarcode").setDisabled(true);
    PCL.getCmp("editBarcode").setDisabled(true);
    PCL.getCmp("buttonSave").setDisabled(true);
    PCL.getCmp("buttonCancel").setText("关闭");
    PCL.getCmp("editBizDT").setReadOnly(true);
    PCL.getCmp("editCustomer").setReadOnly(true);
    PCL.getCmp("editWarehouse").setReadOnly(true);
    PCL.getCmp("editBizUser").setReadOnly(true);
    PCL.getCmp("columnActionDelete").hide();
    PCL.getCmp("columnActionAdd").hide();
    PCL.getCmp("columnActionAppend").hide();
    PCL.getCmp("editReceivingType").setReadOnly(true);
    PCL.getCmp("editBillMemo").setReadOnly(true);

    me.editDealAddress.setReadOnly(true);
  },

  addGoodsByBarCode(goods) {
    if (!goods) {
      return;
    }

    var me = this;
    var store = me.getGoodsGrid().getStore();

    if (store.getCount() == 1) {
      var r = store.getAt(0);
      var id = r.get("goodsId");
      if (id == null || id == "") {
        store.removeAll();
      }
    }

    store.add(goods);
  },

  onEditBarcodeKeydown(field, e) {
    if (e.getKey() == e.ENTER) {
      var me = this;

      var el = PCL.getBody();
      el.mask("查询中...");
      PCL.Ajax.request({
        url: PSI.Const.BASE_URL + "Home/Goods/queryGoodsInfoByBarcode",
        method: "POST",
        params: {
          barcode: field.getValue()
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            var data = PCL.JSON.decode(response.responseText);
            if (data.success) {
              var goods = {
                goodsId: data.id,
                goodsCode: data.code,
                goodsName: data.name,
                goodsSpec: data.spec,
                unitName: data.unitName,
                goodsCount: 1,
                goodsPrice: data.salePrice,
                goodsMoney: data.salePrice,
                taxRate: data.taxRate
              };
              me.addGoodsByBarCode(goods);
              var edit = PCL.getCmp("editBarcode");
              edit.setValue(null);
              edit.focus();
            } else {
              var edit = PCL.getCmp("editBarcode");
              edit.setValue(null);
              me.showInfo(data.msg, () => {
                edit.focus();
              });
            }
          } else {
            me.showInfo("网络错误");
          }
        }

      });
    }
  },

  // xtype:psi_customerfield回调本方法
  // 参见PSI.Customer.CustomerField的onOK方法
  __setCustomerExtData(data) {
    PCL.getCmp("editDealAddress").setValue(data.address_receipt);

    var editWarehouse = PCL.getCmp("editWarehouse");
    if (data.warehouseId) {
      editWarehouse.setIdValue(data.warehouseId);
      editWarehouse.setValue(data.warehouseName);
    }
  }
});
