/**
 * 销售退货入库单-选择销售出库单界面
 * 
 * @author 艾格林门信息服务（大连）有限公司
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Sale.SRSelectWSBillForm", {
  extend: "PSI.AFX.BaseDialogForm",

  /**
   * @override
   */
  initComponent() {
    var me = this;
    PCL.apply(me, {
      header: {
        title: me.formatTitle("选择要退货的销售出库单"),
        height: 40
      },
      width: 1200,
      height: 600,
      layout: "border",
      items: [{
        region: "center",
        border: 0,
        bodyPadding: 10,
        layout: "border",
        items: [{
          region: "north",
          height: "50%",
          split: true,
          layout: "fit",
          items: [me.getWSBillGrid()]
        }, {
          region: "center",
          layout: "fit",
          items: [me.getDetailGrid()]
        }]
      }, {
        region: "north",
        border: 0,
        layout: {
          type: "table",
          columns: 4
        },
        height: 65,
        bodyPadding: 10,
        items: [{
          id: "editWSRef",
          xtype: "textfield",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "销售出库单单号"
        }, {
          xtype: "psi_customerfield",
          showModal: true,
          id: "editWSCustomer",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "客户",
          labelWidth: 60,
          width: 200
        }, {
          id: "editFromDT",
          xtype: "datefield",
          format: "Y-m-d",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务日期（起）"
        }, {
          id: "editToDT",
          xtype: "datefield",
          format: "Y-m-d",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务日期（止）"
        }, {
          xtype: "psi_warehousefield",
          showModal: true,
          id: "editWSWarehouse",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "仓库"
        }, {
          id: "editWSSN",
          xtype: "textfield",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "序列号",
          labelWidth: 60,
          width: 200
        }, {
          xtype: "container",
          items: [{
            xtype: "button",
            text: "查询",
            width: 100,
            margin: "0 0 0 10",
            iconCls: "PSI-button-refresh",
            handler: me.onQuery,
            scope: me
          }, {
            xtype: "button",
            text: "清空查询条件",
            width: 100,
            margin: "0, 0, 0, 10",
            handler: me.onClearQuery,
            scope: me
          }]
        }]
      }],
      listeners: {
        show: {
          fn: me.onWndShow,
          scope: me
        }
      },
      buttons: [{
        text: "选择",
        iconCls: "PSI-button-ok",
        formBind: true,
        handler: me.onOK,
        scope: me
      }, {
        text: "取消",
        handler() {
          me.close();
        },
        scope: me
      }]
    });

    me.callParent(arguments);
  },

  onWndShow() {
    var me = this;
  },

  onOK() {
    var me = this;

    var item = me.getWSBillGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      PSI.MsgBox.showInfo("请选择销售出库单");
      return;
    }
    var wsBill = item[0];
    me.close();
    me.getParentForm().getWSBillInfo(wsBill.get("id"));
  },

  getWSBillGrid() {
    var me = this;

    if (me.__wsBillGrid) {
      return me.__wsBillGrid;
    }

    var modelName = "PSIWSBill_SRSelectForm";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "ref", "bizDate", "customerName",
        "warehouseName", "inputUserName", "bizUserName",
        "amount", "tax", "moneyWithTax"]
    });
    var storeWSBill = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: PSI.Const.BASE_URL + "Home/SaleRej/selectWSBillList",
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      }
    });
    storeWSBill.on("beforeload", () => {
      storeWSBill.proxy.extraParams = me.getQueryParam();
    });

    me.__wsBillGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 50
      }), {
        header: "单号",
        dataIndex: "ref",
        width: 120,
        menuDisabled: true,
        sortable: false
      }, {
        header: "业务日期",
        dataIndex: "bizDate",
        menuDisabled: true,
        sortable: false,
        width: 80,
        align: "center"
      }, {
        header: "客户",
        dataIndex: "customerName",
        width: 200,
        menuDisabled: true,
        sortable: false
      }, {
        header: "销售金额(不含税)",
        dataIndex: "amount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 130
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "出库仓库",
        dataIndex: "warehouseName",
        menuDisabled: true,
        sortable: false,
        width: 150
      }, {
        header: "业务员",
        dataIndex: "bizUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "录单人",
        dataIndex: "inputUserName",
        menuDisabled: true,
        sortable: false
      }],
      listeners: {
        select: {
          fn: me.onMainGridSelect,
          scope: me
        },
        itemdblclick: {
          fn: me.onOK,
          scope: me
        }
      },
      store: storeWSBill,
      bbar: [{
        id: "srbill_selectform_pagingToobar",
        xtype: "pagingtoolbar",
        border: 0,
        store: storeWSBill
      }, "-", {
        xtype: "displayfield",
        value: "每页显示"
      }, {
        id: "srbill_selectform_comboCountPerPage",
        xtype: "combobox",
        editable: false,
        width: 60,
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["text"],
          data: [["20"], ["50"], ["100"], ["300"],
          ["1000"]]
        }),
        value: 20,
        listeners: {
          change: {
            fn() {
              storeWSBill.pageSize = PCL.getCmp("srbill_selectform_comboCountPerPage").getValue();
              storeWSBill.currentPage = 1;
              PCL.getCmp("srbill_selectform_pagingToobar").doRefresh();
            },
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        value: "条记录"
      }]
    });

    return me.__wsBillGrid;
  },

  onQuery() {
    PCL.getCmp("srbill_selectform_pagingToobar").doRefresh();
  },

  getQueryParam() {
    var result = {};

    var ref = PCL.getCmp("editWSRef").getValue();
    if (ref) {
      result.ref = ref;
    }

    var customerId = PCL.getCmp("editWSCustomer").getIdValue();
    if (customerId) {
      result.customerId = customerId;
    }

    var warehouseId = PCL.getCmp("editWSWarehouse").getIdValue();
    if (warehouseId) {
      result.warehouseId = warehouseId;
    }

    var fromDT = PCL.getCmp("editFromDT").getValue();
    if (fromDT) {
      result.fromDT = PCL.Date.format(fromDT, "Y-m-d");
    }

    var toDT = PCL.getCmp("editToDT").getValue();
    if (toDT) {
      result.toDT = PCL.Date.format(toDT, "Y-m-d");
    }

    var sn = PCL.getCmp("editWSSN").getValue();
    if (sn) {
      result.sn = sn;
    }

    return result;
  },

  onClearQuery() {
    PCL.getCmp("editWSRef").setValue(null);
    PCL.getCmp("editWSCustomer").clearIdValue();
    PCL.getCmp("editWSWarehouse").clearIdValue();
    PCL.getCmp("editFromDT").setValue(null);
    PCL.getCmp("editToDT").setValue(null);
    PCL.getCmp("editWSSN").setValue(null);

    this.onQuery();
  },

  getDetailGrid() {
    var me = this;

    if (me.__detailGrid) {
      return me.__detailGrid;
    }

    var modelName = "SRSelectWSBillForm_PSIWSBillDetail";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsCode", "goodsName", "goodsSpec",
        "unitName", "goodsCount", "goodsMoney",
        "goodsPrice", "sn", "memo", "taxRate", "tax",
        "moneyWithTax", "goodsPriceWithTax", "rejGoodsCount", "realGoodsCount"]
    });
    var store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me.__detailGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      viewConfig: {
        enableTextSelection: true
      },
      title: "销售出库单明细",
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 50
      }), {
        header: "商品编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false
      }, {
        header: "品名/规格型号",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        width: 90
      }, {
        header: "退货数量",
        width: 90,
        dataIndex: "rejGoodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }, {
        header: "实际出库数量",
        width: 90,
        dataIndex: "realGoodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        width: 60,
        align: "center"
      }, {
        header: "单价(不含税)",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "销售金额(不含税)",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 110
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "#",
        width: 60
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "含税价",
        dataIndex: "goodsPriceWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "序列号",
        dataIndex: "sn",
        menuDisabled: true,
        sortable: false
      }, {
        header: "备注",
        dataIndex: "memo",
        width: 200,
        menuDisabled: true,
        sortable: false
      }],
      store: store
    });

    return me.__detailGrid;
  },

  onMainGridSelect() {
    var me = this;
    me.getDetailGrid().setTitle("销售出库单明细");

    me.refreshDetailGrid();
  },

  refreshDetailGrid() {
    var me = this;
    me.getDetailGrid().setTitle("销售出库单明细");
    var grid = me.getWSBillGrid();
    var item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    var bill = item[0];

    grid = me.getDetailGrid();
    grid.setTitle("单号: " + bill.get("ref") + " 客户: "
      + bill.get("customerName") + " 出库仓库: "
      + bill.get("warehouseName"));
    var el = grid.getEl();
    el.mask(PSI.Const.LOADING);

    var r = {
      url: PSI.Const.BASE_URL + "Home/SaleRej/wsBillDetailListForSRBill",
      params: {
        billId: bill.get("id")
      },
      method: "POST",
      callback(options, success, response) {
        var store = grid.getStore();

        store.removeAll();

        if (success) {
          var data = PCL.JSON.decode(response.responseText);
          store.add(data);
        }

        el.unmask();
      }
    };

    PCL.Ajax.request(r);
  }
});
